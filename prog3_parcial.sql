-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-06-2021 a las 05:15:51
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prog3_parcial`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

CREATE TABLE `paises` (
  `id` int(11) NOT NULL,
  `nombre` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `codigo_dane` char(50) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `tipo_gobierno` varchar(250) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `num_poblacion` int(11) NOT NULL,
  `moneda` varchar(250) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `num_Departamentos` int(11) NOT NULL,
  `sectorEconomico` varchar(250) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `paises`
--

INSERT INTO `paises` (`id`, `nombre`, `codigo_dane`, `tipo_gobierno`, `num_poblacion`, `moneda`, `num_Departamentos`, `sectorEconomico`) VALUES
(1, 'daniel', '23252', 'herradas', 222222, 'peso', 252, 'sasas'),
(2, 'daniel', '23252', 'herradas', 222222, 'peso', 252, 'sasas'),
(3, '', '', '', 0, '', 0, ''),
(4, '', '', '', 0, '', 0, ''),
(5, 'danii', '1252', 'hereditarios', 0, 'peos', 7, 'pobresa'),
(6, 'sasa', '121313', 'asasa', 255, 'sadaddd', 10, 'asasasaassa'),
(7, 'sasa', '121313', 'asasa', 255, 'sadaddd', 10, 'asasasaassa'),
(8, 'sasa', '121313', 'asasa', 255, 'sadaddd', 10, 'asasasaassa'),
(9, '', '', '', 0, '', 0, ''),
(10, 'sasa', '121313', 'asasa', 255, 'sadaddd', 10, 'asasasaassa'),
(11, 'sasa', '121313', 'asasa', 255, 'sadaddd', 10, 'asasasaassa'),
(12, 'sasa', '121313', 'asasa', 255, 'sadaddd', 10, 'asasasaassa'),
(13, 'sasa', '121313', 'asasa', 255, 'sadaddd', 10, 'asasasaassa'),
(14, '', '', '', 0, '', 0, ''),
(15, '', '', '', 0, '', 0, ''),
(16, 'sasa', '121313', 'asasa', 255, 'sadaddd', 10, 'asasasaassa'),
(17, 'sasa', '121313', 'asasa', 255, 'sadaddd', 10, 'asasasaassa'),
(18, 'sasa', '121313', 'asasa', 255, 'sadaddd', 10, 'asasasaassa');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `paises`
--
ALTER TABLE `paises`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `paises`
--
ALTER TABLE `paises`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
